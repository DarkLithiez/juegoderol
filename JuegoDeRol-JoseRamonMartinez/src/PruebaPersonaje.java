import org.junit.*;
import junit.framework.TestCase;



public class PruebaPersonaje extends TestCase {
	public Personaje personaje;
	@Before
	public void setUp() {
		personaje=new Personaje();
	}
	@Test
	public void testSetNombre(){
		personaje.setNombre("Jose Ramon");
		assertTrue(personaje.getNombre().equals("Jose Ramon"));
	}
	@Test
	public void testSetEdad(){
		personaje.setEdad(25);
		assertEquals(personaje.getEdad(),25);
	}
	@Test
	public void testSetVida(){
		assertEquals(personaje.getVida(),200);
		personaje.setVida(20);
		assertEquals(personaje.getVida(),180);
		assertEquals(personaje.getVida2(),"180");

	}
	@Test
	public void testGetDaño(){
		assertEquals(personaje.getDaño(),10);
	}

}
