import org.junit.*;

import junit.framework.TestCase;

import org.junit.Test;

public class PruebaMonstruo extends TestCase{
	public Monstruo monstruo;
	@Before
	public void setUp() {
		monstruo=new Monstruo();
	}
	@Test
	public void testSetNombre(){
		monstruo.setNombre("Jose Ramon");
		assertTrue(monstruo.getNombre().equals("Jose Ramon"));
	}
	@Test
	public void testSetVida(){
		assertEquals(monstruo.getVida(),200);
		monstruo.setVida(20);
		assertEquals(monstruo.getVida(),180);
		assertEquals(monstruo.getVida2(),"180");

	}
	@Test
	public void testGetDaño(){
		assertEquals(monstruo.getDaño(),10);
	}

}
