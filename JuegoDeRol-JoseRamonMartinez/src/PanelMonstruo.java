import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
/**
 * Esta clase se encarga de dibujar el panel de monstruo con todas sus imagenes botones etc.
 * @author JoseRamonMartinezMartinez
 * @version 4 Final
 *@param monstruo se crea un monstruo vacio
 *@param barraVida se crea un jlabel para mostrar la vida en el panel
 */

public class PanelMonstruo extends JPanel {

	/**
	 * Create the panel.
	 */
	private Monstruo monstruo;
	private JLabel barraVida;
	public PanelMonstruo(Monstruo monstruo){
		this.monstruo=monstruo;
		setForeground(Color.WHITE);
		setBackground(Color.WHITE);
		setLayout(null);
		setBounds(100,100,217,350);

		JLabel lblNewLabel = new JLabel("NOMBRE:");
		lblNewLabel.setBounds(12, 0, 70, 15);
		add(lblNewLabel);

		JLabel nombreMonstruo = new JLabel("");
		nombreMonstruo.setBounds(81, 0, 81, 15);
		add(nombreMonstruo);
		nombreMonstruo.setText(monstruo.getNombre());

		JLabel lblNewLabel_2 = new JLabel("VIDA:");
		lblNewLabel_2.setBounds(12, 23, 48, 15);
		add(lblNewLabel_2);

		JLabel AvatarMonstruo = new JLabel("");
		AvatarMonstruo.setIcon(new ImageIcon(PanelMonstruo.class.getResource("/Imagenes/AvatarMonstruo.gif")));
		AvatarMonstruo.setBounds(12, 101, 150, 178);
		add(AvatarMonstruo);
		
		JLabel evento = new JLabel("");
		evento.setBounds(12, 310, 150, 15);
		add(evento);
		
		barraVida = new JLabel();
		barraVida.setBounds(63, 21, 70, 19);
		add(barraVida);
		//barraVida.setColumns(10);
		barraVida.setText(monstruo.getVida2());
	}
	/**
	 * Cambias el valor del label de barra vida
	 */
	public void setBarraVida(){
		barraVida.setText(monstruo.getVida2());
	}
	/**
	 * actualizas el valor del label para llamar a este metodo cuando atacas y se tenga que cambiar dicho valor
	 */
	public void update(){
		setBarraVida();
	}

}
