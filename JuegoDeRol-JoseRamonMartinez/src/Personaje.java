/**
 * Esta clase construye el personaje y crea todos sus metodos para editarlo
 * @author JoseRamonMartinezMartinez
 * @version 4 Final
 * @param nombre campo del nombre del personaje de tipo String
 * @param edad campo de la edad del personaje de tipo int
 * @param vida campo de la vida del personaje de tipo int
 * @param daño campo del daño del personaje de tipo int
 *
 */
public class Personaje {
	private String nombre;
	private int edad;
	private int vida;
	private int daño;
	/**
	 * El constructor de Personaje
	 */
	public Personaje(){
		nombre="personaje";
		edad=0;
		vida=200;
		daño=10;
	}
	/**
	 * 
	 * @return devuelve el nombre del personaje
	 */
	public String getNombre(){
		return nombre;
	}
	/**
	 * Cambia el nombre del personaje por el que se pase como parametro
	 * @param newNombre
	 */
	public void setNombre(String newNombre){
		nombre=newNombre;
	}
	/**
	 * 
	 * @return devuelve la edad del personaje
	 */
		
	public int getEdad(){
		return edad;
	}
	/**
	 * Cambia la edad del personaje por la que pases por parametro
	 * @param newEdad
	 */
	public void setEdad(int newEdad){
		edad=newEdad;
	}
	/**
	 * 
	 * @return Devulve la vida del personaje de tipo int
	 */
	public int getVida(){
		return vida;
	}
	/**
	 * 
	 * @return devuelve la vida del personaje de tipo String
	 */
	public String getVida2(){
		return ""+vida+"";
	}
	/**
	 * 
	 * @return devuelve el daño del personaje
	 */
	public int getDaño(){
		return daño;
	}
	/**
	 * Cambias la vida del personaje pasandole el daño como parametro
	 * @param daño
	 */
	public void setVida(int daño){
		vida=vida-daño;
	}
}
