import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

import java.awt.Color;
import javax.swing.JButton;
import javax.swing.JTextField;
/**
 * Esta clase se encarga de dibujar el panel donde estaran todos los botones imagenes y textfields de personaje.
 * @author JoseRamonMartinezMartinez
 * @version 4 Final
 * @param personaje1 se crea un personaje vacio
 * @param barraVida se crea un panel para mostrar la vida.
 *
 */

public class PanelPersonaje extends JPanel {
	
	 private Personaje personaje1;
	 private JLabel barraVida;
	 /**
	  * Es el contructor de la clase y se le pasa un personaje como parametro que anteriorme ha sido creado en creacion.
	  * @param personaje
	  */
	public PanelPersonaje(Personaje personaje) {
		this.personaje1=personaje;
		setBackground(Color.WHITE);
		setForeground(Color.WHITE);
		setLayout(null);
		setBounds(100,100,217,350);
		
		JLabel nombrePersonaje = new JLabel("");
		nombrePersonaje.setForeground(Color.BLACK);
		nombrePersonaje.setBounds(103, 16, 78, 15);
		add(nombrePersonaje);
		nombrePersonaje.setText(personaje1.getNombre());
		
		JLabel Edad = new JLabel("New label");
		Edad.setForeground(Color.BLACK);
		Edad.setBounds(170, -1, 20, 15);
		add(Edad);
		Edad.setText(Integer.toString(personaje1.getEdad()));
		
		JLabel lblNewLabel = new JLabel("VIDA:");
		lblNewLabel.setForeground(Color.BLACK);
		lblNewLabel.setBackground(Color.WHITE);
		lblNewLabel.setBounds(22, -1, 44, 15);
		add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("NOMBRE:");
		lblNewLabel_1.setForeground(Color.BLACK);
		lblNewLabel_1.setBounds(22, 16, 70, 15);
		add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("EDAD:");
		lblNewLabel_2.setForeground(Color.BLACK);
		lblNewLabel_2.setBounds(122, -1, 44, 15);
		add(lblNewLabel_2);
		
		JLabel AvatarPersonaje = new JLabel("");
		AvatarPersonaje.setIcon(new ImageIcon(PanelPersonaje.class.getResource("/Imagenes/PersonajeAvatar2.png")));
		AvatarPersonaje.setBounds(22, 63, 159, 241);
		add(AvatarPersonaje);
		
		JLabel evento = new JLabel("");
		evento.setBounds(22, 310, 159, 15);
		add(evento);
		
		barraVida = new JLabel();
		barraVida.setBounds(67, -3, 44, 17);
		add(barraVida);
		//barraVida.setColumns(10);
		barraVida.setText(personaje1.getVida2());
	}
	/**
	 * modifica la vida que aparece en el panel de barravida
	 */
	public void setBarraVida(){
		barraVida.setText(personaje1.getVida2());
	}
	/**
	 * actualiza el label de barra vida en cada ataque.
	 */
	
	public void update(){
		setBarraVida();
	}
	
}
