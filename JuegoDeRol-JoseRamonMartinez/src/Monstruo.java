/**
 * 
 * @author JoseRamonMartinezMartinez.
 * @version 4 Final.
 * @param nombre campo de tipo String del nombre del monstruo.
 * @param vida	campo de tipo int donde se almacena la vida.
 * @param daño campo de tipo int donde se almacena el daño.
 *
 */
public class Monstruo {

	private String nombre;
	private int vida;
	private int daño;
	/**
	 * Este metodo es el constructor y da valor a los atributos.
	 */
	
	public Monstruo(){
		nombre="monster";
		daño=10;
		vida=200;
	}
	/**
	 * @return devuelve el nombre del monstruo.
	 */
	public String getNombre(){
		return nombre;
	}
	/**
	 * Este metodo cambia el nombre del monstruo al monstruo que se le pase como parametro;
	 * @param newNombre nombre al que quieres cambiar.
	 */
	public void setNombre(String newNombre){
		nombre=newNombre;
	}
	/**
	 * 
	 * @return devuelve la vida del monstruo.
	 */
	public int getVida(){
		return vida;
	}
	/**
	 * 
	 * @return devuelve la vida del monstruo pero en tipo String.
	 */
	public String getVida2(){
		return ""+vida+"";
	}
	/**
	 * Cambia la vida dependiendo del daño que se le pase.
	 * @param daño
	 */
	public void setVida(int daño){
		vida=vida-daño;
	}
	/**
	 * @return Devuelve el daño.
	 */
	public int getDaño(){
		return daño;
	}

}

