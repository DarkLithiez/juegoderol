import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Color;
/**
 * Esta clase inicia una pantalla de creacion de personaje y cuando rellenas los campos carga el entorno
 * del juego.
 * @author JoseRamonMartinezMartinez
 * @version 4 Final
 * @param contentPanel el panel donde se construye el personaje
 * @param nombrePj el campo donde se escribe el nombre del personaje
 * @param edad campo donde se escribe la edad
 * 
 */

public class Creacion extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField nombrePj;
	private JTextField edad;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Creacion dialog = new Creacion();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Este es el constructor de la clase y el encargado de crear todos los paneles.
	 * 
	 */
	public Creacion() {
		setBounds(100, 100, 360, 250);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblEdad = new JLabel("Edad:");
		lblEdad.setForeground(Color.WHITE);
		lblEdad.setBounds(158, 122, 53, 15);
		contentPanel.add(lblEdad);
		{
			JLabel lblBievenidoAJuegoderol = new JLabel("Bievenido a JuegoDeRol Version 1.0");
			lblBievenidoAJuegoderol.setForeground(Color.WHITE);
			lblBievenidoAJuegoderol.setBounds(55, 12, 250, 15);
			contentPanel.add(lblBievenidoAJuegoderol);
		}
		
		JLabel lblCreacionDePersonaje = new JLabel("CREACION DE PERSONAJE");
		lblCreacionDePersonaje.setForeground(Color.WHITE);
		lblCreacionDePersonaje.setBounds(99, 46, 250, 15);
		contentPanel.add(lblCreacionDePersonaje);
		
		nombrePj = new JTextField();
		nombrePj.setBounds(126, 91, 114, 19);
		contentPanel.add(nombrePj);
		nombrePj.setColumns(10);
		
		edad = new JTextField();
		edad.setBounds(152, 149, 53, 19);
		contentPanel.add(edad);
		edad.setColumns(10);
		
		JLabel lblNombre = new JLabel("Nombre:");
		lblNombre.setForeground(Color.WHITE);
		lblNombre.setBounds(152, 73, 70, 15);
		contentPanel.add(lblNombre);
		
		JLabel fondoCreacion = new JLabel("New label");
		fondoCreacion.setIcon(new ImageIcon(Creacion.class.getResource("/Imagenes/FondoCreacion.jpg")));
		fondoCreacion.setBounds(0, 0, 358, 188);
		contentPanel.add(fondoCreacion);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("OK");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Personaje person = new Personaje();
								person.setNombre(nombrePj.getText());
								person.setEdad(Integer.parseInt(edad.getText()));
						PanelPersonaje personaje=new PanelPersonaje(person);
						Monstruo monster = new Monstruo();
						PanelMonstruo monstruo=new PanelMonstruo(monster);
						Entorno entorno=new Entorno(person);
						dispose();
						entorno.setVisible(true);
					}
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						System.exit(1);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
}
