import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Color;

import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * Esta clase se encarga de crear el Jframe en el que se cargaran los jpanel personaje y monstruo.
 * @author JoseRamonMartinezMartinez
 * @version 4 Final
 *@param contentPane crea el panel
 *@param personaje crea un personaje vacio
 *@param monstruo crea un monstruo vacio
 *@param panel dibuja al personaje en pantalla
 *@param panel_1 dibuja al monstruo en pantalla
 *
 */

public class Entorno extends JFrame {

	private JPanel contentPane;
	
	private Personaje personaje;
	private Monstruo monstruo;
	PanelPersonaje panel;
	PanelMonstruo panel_1;
	
	/**
	 * Este metodo define 2 tiradas de daño dependiendo del daño de personaje y monstruo y le da un valor aleatorio
	 * y le resta la vida obtenida en la tiradas a monstruo y personaje.
	 * @param tiradaPj
	 * @param tiradaMons
	 */

	public void atacar(){
		int tiradaPj=(int)(Math.random()*personaje.getDaño()+1);
		int tiradaMons=(int)(Math.random()*monstruo.getDaño()+1);
		personaje.setVida(tiradaMons);
		monstruo.setVida(tiradaPj);
		repaint();
	
	}
	
	/**
	 * Esta clase se encarga de dibujar el jframe en el que iran ubicados el panel de personaje y monstruo
	 * @param personaje se le pasa el personaje que se ha creado en la clase creacion.
	 */
	public Entorno(Personaje personaje) {
		this.personaje=personaje;
		monstruo=new Monstruo();
		monstruo.setNombre("Zombie");
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 500);
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		contentPane.setBackground(Color.WHITE);
		setContentPane(contentPane);

		 panel = new PanelPersonaje(personaje);
		contentPane.add(panel, BorderLayout.WEST);
		Dimension preferredSize=new Dimension(217,350);
		panel.setPreferredSize(preferredSize);
		
		panel_1 = new PanelMonstruo(monstruo);
		contentPane.add(panel_1, BorderLayout.EAST);
		Dimension preferredSize1=new Dimension(217,350);
		panel_1.setPreferredSize(preferredSize1);
	
		
		JButton Atacar = new JButton("ATACAR");
		Atacar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				gameOver();
				atacar();
				panel.setBarraVida();
				panel_1.setBarraVida();
			}
		});
		contentPane.add(Atacar, BorderLayout.SOUTH);
		Dimension PreferredSize2=new Dimension(50,50);
		Atacar.setPreferredSize(PreferredSize2);

	}
	/**
	 * Comprueba si la vida de personaje o monstruo es igual o menos a 0 y dependiendo de este resultado
	 * devuelve un panel de gameover o victoria.
	 */
	public void gameOver(){
		if(personaje.getVida()<=0){
			int n = JOptionPane.showConfirmDialog(
		            null,
		            "¿Quieres volver a empezar?",
		            "GAME OVER",
		            JOptionPane.YES_NO_OPTION);

		        if(n==JOptionPane.YES_OPTION){
		            Creacion.main(null);
		        }
		        else {
		            System.exit(ABORT);
		        }
		}
		if(monstruo.getVida()<=0){
			int n = JOptionPane.showConfirmDialog(
		            null,
		            "¿Quieres volver a empezar?",
		            "YOU WIN",
		            JOptionPane.YES_NO_OPTION);

		        if(n==JOptionPane.YES_OPTION){
		            Creacion.main(null);
		        }
		        else {
		            System.exit(ABORT);
		        }
			
		}
	}
		
}
	

